package com.develop.totvsapp.models

data class Product(
    val title: String,
    val image: String,
    val idProd: String,
    val qtd: String,
    val preco: String
)