package com.develop.totvsapp.presentation.registration

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.develop.totvsapp.R
import com.develop.totvsapp.presentation.intro.IntroView
import kotlinx.android.synthetic.main.activity_login.*
import com.google.firebase.auth.FirebaseAuth


class LoginView: AppCompatActivity(), LoginContract.View {
    lateinit var presenter: LoginPresenter
    lateinit var mAuth: FirebaseAuth
    lateinit var uidUser: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = LoginPresenter(this)
        mAuth = FirebaseAuth.getInstance()

        edUser.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                presenter.textChanged(p0.toString(),edSenha.text.toString())
            }

        })

        edSenha.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                presenter.textChanged(edUser.text.toString(),p0.toString())
            }

        })

        btnLogin.setOnClickListener {
            presenter.processLogin(edUser.text.toString(), edSenha.text.toString())
        }

        btnCancel.setOnClickListener {
            finish()
        }

    }

    override fun statusButton(status: Boolean) {
        btnLogin.isEnabled = true
    }

    override fun setUser(uid: String) {
        uidUser = uid
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = mAuth.currentUser
        //updateUI(currentUser)
    }

    override fun showMessage(msg: String) {
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show()
    }

    override fun navigate() {
        var i = Intent(this, IntroView::class.java)
        i.putExtra("uidUser",uidUser)
        startActivity(i)
        finish()
    }


    override fun getLifeCycleView(): Lifecycle {
        return lifecycle
    }

    override fun getLifeCycleOwnerView(): LifecycleOwner {
        return this
    }

    override fun getActivityView(): FragmentActivity {
        return this
    }
}