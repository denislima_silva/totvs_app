package com.develop.totvsapp.presentation.comanda

import androidx.lifecycle.ViewModel
import com.develop.totvsapp.models.Product

class ComandaData: ViewModel() {
    var productsComanda: ArrayList<Product> = arrayListOf()
}