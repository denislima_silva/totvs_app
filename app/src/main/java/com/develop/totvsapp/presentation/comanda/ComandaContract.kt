package com.develop.totvsapp.presentation.comanda

import com.develop.totvsapp.base.TotvsAppBaseContractPresenter
import com.develop.totvsapp.base.TotvsAppBaseContractView
import com.develop.totvsapp.models.Product
import com.develop.totvsapp.repositories.IntroRepositoryFromFile

class ComandaContract {
    interface View: TotvsAppBaseContractView{
        fun loadProductsComanda(list: ArrayList<Product>,uidUser: String)
        fun deleteProductComanda(uidUser: String,idProd: String)
        fun setPrecoTot(preco: Int)
    }
    interface Presenter: TotvsAppBaseContractPresenter{
        fun getProductsComanda(uidUser: String)
        fun processDeleteProductComanda(uidUser: String,idProd: String)
        fun processPaymentComanda(uidUser: String)
        fun processSum(list: ArrayList<Product>)
    }
}