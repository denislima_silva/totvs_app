package com.develop.totvsapp.presentation.details

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.develop.totvsapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*
import androidx.appcompat.app.AlertDialog
import com.develop.totvsapp.presentation.comanda.ComandaView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class DetailsView: AppCompatActivity(), DetailsContract.View {

    lateinit var presenter: DetailsPresenter
    var precoTot: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        presenter = DetailsPresenter(this)

        val extras = intent.extras
        var imageDetails: String = ""
        val desc: String?
        var uidUser: String = ""
        var idProd: String = ""
        var title: String = ""
        var preco: String = ""

        if (extras != null) {
            imageDetails = extras.getString("imageDetails")
            desc = extras.getString("desc")
            uidUser = extras.getString("uidUser")
            idProd = extras.getString("idProd")
            title = extras.getString("title")
            preco = extras.getString("preco")


            Picasso.get().load(imageDetails).into(imgDetailsTop)
            tvDetails.text = desc
            tvUnitario.text = "R$ : 0"

        }

        edQtd.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                presenter.textChanged(p0.toString(),preco)
            }

        })

        btnVoltar.setOnClickListener {
            presenter.processClose()
        }

        btnAdd.setOnClickListener {

            presenter.addProductComanda(uidUser,idProd,imageDetails,title,precoTot,edQtd.text.toString().toInt())

            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setTitle("Item adicionado com sucesso!")
            alertDialogBuilder.setMessage("Gostaria de adicionar mais itens ?")
            alertDialogBuilder.setPositiveButton("Sim") { dialog, which ->
                presenter.processClose()
            }
            alertDialogBuilder.setNegativeButton("Não")  { dialog, which ->
                var i = Intent(this,ComandaView::class.java)
                i.putExtra("uidUser",uidUser)
                startActivity(i)
                presenter.processClose()
            }
            alertDialogBuilder.show()

        }

    }

    override fun statusButton(status: Boolean,tot: Int){
        tvUnitario.text = "R$ : "+tot
        precoTot = tot
        btnAdd.isEnabled = status
    }

    override fun closeActivity() {
        finish()
    }

    override fun getLifeCycleView(): Lifecycle {
        return lifecycle
    }

    override fun getLifeCycleOwnerView(): LifecycleOwner {
        return this
    }

    override fun getActivityView(): FragmentActivity {
        return this
    }
}