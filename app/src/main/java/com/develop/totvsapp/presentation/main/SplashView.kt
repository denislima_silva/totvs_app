package com.develop.totvsapp.presentation.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.develop.totvsapp.LoginActivity
import com.develop.totvsapp.R
import com.develop.totvsapp.presentation.registration.LoginView

class SplashView: AppCompatActivity(), SplashContract.View {
    lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash)

        presenter = SplashPresenter(this)

        Handler().postDelayed(Runnable { presenter.processNavigate() }, 2000)

    }

    override fun navigate() {
        var i = Intent(this, LoginView::class.java)
        startActivity(i)
        finish()
    }

    override fun getLifeCycleView(): Lifecycle {
        return lifecycle
    }

    override fun getLifeCycleOwnerView(): LifecycleOwner {
        return this
    }

    override fun getActivityView(): FragmentActivity {
        return this
    }
}