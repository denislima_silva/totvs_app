package com.develop.totvsapp.presentation.registration

import com.develop.totvsapp.base.TotvsAppBaseContractPresenter
import com.develop.totvsapp.base.TotvsAppBaseContractView

class LoginContract {
    interface View: TotvsAppBaseContractView{
        fun navigate()
        fun showMessage(msg: String)
        fun statusButton(status: Boolean)
        fun setUser(uid: String)
    }
    interface Presenter: TotvsAppBaseContractPresenter{
        fun processLogin(user: String, pass: String)
        fun textChanged(txt1: String, txt2: String)
    }
}