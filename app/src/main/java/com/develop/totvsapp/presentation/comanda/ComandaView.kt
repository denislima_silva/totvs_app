package com.develop.totvsapp.presentation.comanda

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import com.develop.totvsapp.R
import com.develop.totvsapp.models.Product
import com.develop.totvsapp.presentation.main.Constants
import com.develop.totvsapp.repositories.IntroRepositoryFromFile
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_comanda.*
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_intro.*

class ComandaView: AppCompatActivity(), ComandaContract.View {

    lateinit var presenter: ComandaPresenter
    var recipeList = ArrayList<IntroRepositoryFromFile>()
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comanda)


        btnVoltarComanda.setOnClickListener {
            finish()
        }

        presenter = ComandaPresenter(this)

        val extras = intent.extras
        var uidUser: String = ""

        if (extras != null) {
            uidUser = extras.getString("uidUser")
        }

        presenter.getProductsComanda(uidUser)

        btnPagarComanda.setOnClickListener {
            var alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setTitle("Deseja pagar a comanda ?")
            alertDialogBuilder.setMessage("Isso vai limpar todos os itens dessa comanda")
            alertDialogBuilder.setPositiveButton("Sim") { dialog, which ->
                presenter.processPaymentComanda(uidUser)
            }
            alertDialogBuilder.setNegativeButton("Não") { dialog, which ->

            }
            alertDialogBuilder.show()
        }

    }

    override fun setPrecoTot(preco: Int) {
        tvComandaTotal.text = "Total R$ : "+preco.toString()
    }

    override fun loadProductsComanda(list: ArrayList<Product>, uidUser: String) {

        presenter.processSum(list)

        recyclerViewComanda.adapter = ComandaAdapter(list, this,uidUser)
        var layoutManager = LinearLayoutManager(applicationContext)
        recyclerViewComanda.layoutManager = layoutManager
    }

    override fun deleteProductComanda(uidUser: String,idProd: String){

        presenter = ComandaPresenter(this)
        presenter.processDeleteProductComanda(uidUser,idProd)

    }

    override fun getLifeCycleView(): Lifecycle {
        return lifecycle
    }

    override fun getLifeCycleOwnerView(): LifecycleOwner {
        return this
    }

    override fun getActivityView(): FragmentActivity {
        return this
    }
}