package com.develop.totvsapp.presentation.details

import com.develop.totvsapp.base.TotvsAppBaseContractPresenter
import com.develop.totvsapp.base.TotvsAppBaseContractView

class DetailsContract {
    interface View: TotvsAppBaseContractView{
        fun closeActivity()
        fun statusButton(status: Boolean,tot: Int)
    }
    interface Presenter: TotvsAppBaseContractPresenter{
        fun processClose()
        fun textChanged(txt: String,preco: String)
        fun addProductComanda(uidUser: String,idProd: String,imageDetails: String,title: String,precoTot: Int,qtd: Int)
    }
}