package com.develop.totvsapp.presentation.main

class SplashPresenter(var view: SplashView) : SplashContract.Presenter {

    override fun processNavigate() {
        view.navigate()
    }

}