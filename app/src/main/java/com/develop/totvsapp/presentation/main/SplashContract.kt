package com.develop.totvsapp.presentation.main

import com.develop.totvsapp.base.TotvsAppBaseContractPresenter
import com.develop.totvsapp.base.TotvsAppBaseContractView

class SplashContract {
    interface View: TotvsAppBaseContractView {
        fun navigate()
    }
    interface Presenter: TotvsAppBaseContractPresenter{
        fun processNavigate()
    }
}