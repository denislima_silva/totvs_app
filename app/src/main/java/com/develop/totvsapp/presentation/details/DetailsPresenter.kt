package com.develop.totvsapp.presentation.details

import com.develop.totvsapp.repositories.ComandaRepository

class DetailsPresenter(var view: DetailsContract.View): DetailsContract.Presenter {

    var comandaRepository: ComandaRepository

    init {

        comandaRepository = ComandaRepository()
    }

    override fun processClose() {
        view.closeActivity()
    }

    override fun textChanged(txt: String,preco: String) {

        if (txt.isNotEmpty()){

            var qtd = txt.toInt()
            var p = preco.toInt()
            var tot = qtd * p

            view.statusButton(true,tot)


        }
        else{
            view.statusButton(false,0)
        }

    }

    override fun addProductComanda(uidUser: String, idProd: String, img: String,title: String,precoTot: Int,qtd: Int) {
        comandaRepository.addProductComandaModel(uidUser, idProd, img,title,precoTot,qtd)
    }

}