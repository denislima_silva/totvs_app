package com.develop.totvsapp.presentation.main

import android.os.StrictMode
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDexApplication
import com.develop.totvsapp.di.ComponentApplication


class TotvsApp: MultiDexApplication() {
    lateinit var componentApplication: ComponentApplication
        private set

    override fun onCreate() {
        super.onCreate()
        instance = this

        if (BuildConfig.DEBUG) {

        }

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        //componentApplication = DaggerComponentApplication.builder().build()
        //componentApplication.inject
        componentApplication.inject(this)
    }

    companion object {
        lateinit var instance: TotvsApp
            private set
    }
}