package com.develop.totvsapp.presentation.comanda

import com.develop.totvsapp.models.Product
import com.develop.totvsapp.repositories.ComandaRepository
import com.google.firebase.database.*

class ComandaPresenter(var view: ComandaContract.View): ComandaContract.Presenter {

    private var comandaRepository: ComandaRepository
    private lateinit var database: DatabaseReference
    lateinit var productsComandaList: ArrayList<Product>


    init {

        comandaRepository = ComandaRepository()

    }


    override fun getProductsComanda(uiUser: String) {

        database = FirebaseDatabase.getInstance().getReference().child("comanda").child(uiUser)

        productsComandaList = arrayListOf()

        database.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                var newList: ArrayList<Product> = arrayListOf()
                val children = p0!!.children

                if (productsComandaList.size > 0){
                    productsComandaList.clear()
                }

                children.forEach {
                    //println(it.toString())
                    //println(it.key)

                    productsComandaList.add(
                        Product(
                            it.child("title").getValue().toString(),
                            it.child("image").getValue().toString(),
                            it.key.toString(),
                            it.child("qtd").getValue().toString(),
                            it.child("preco").getValue().toString()
                        )
                    )
                }

                view.loadProductsComanda(productsComandaList,uiUser)

            }

        })
    }

    override fun processDeleteProductComanda(uidUser: String, idProd: String) {
        comandaRepository.deleteProductComanda(uidUser,idProd)
    }

    override fun processPaymentComanda(uidUser: String) {
        comandaRepository.paymentComanda(uidUser)
    }

    override fun processSum(list: ArrayList<Product>) {

        var tot: Int = 0

        list.forEach {

            if (it.preco.toString() != "null"){
                tot += it.preco.toInt()
                //println(it.preco)
            }


        }

        view.setPrecoTot(tot)

    }

}