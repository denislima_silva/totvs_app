package com.develop.totvsapp.presentation.registration

import com.google.firebase.auth.AuthResult
import com.google.android.gms.tasks.OnCompleteListener
import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginPresenter(var view: LoginView) : LoginContract.Presenter {

    @Inject
    internal lateinit var context: Context

    private lateinit var mAuth: FirebaseAuth

    override fun processLogin(email: String, pass: String) {
        mAuth = FirebaseAuth.getInstance()
        mAuth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener(view.getActivityView(), OnCompleteListener<AuthResult> { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithEmail:success")
                    val user = mAuth.getCurrentUser()
                    //view.showMessage(user!!.uid)
                    view.setUser(user!!.uid)
                    view.navigate()

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    view.showMessage("Email ou senha incorretos, favor verificar!")
                }

            })

    }

    override fun textChanged(txt1: String, txt2: String) {

        if (txt1.isNotEmpty() && txt2.isNotEmpty()){
            view.statusButton(true)
        }
        else{
            view.statusButton(false)
        }

    }

}