package com.develop.totvsapp.presentation.comanda

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.develop.totvsapp.R
import com.develop.totvsapp.models.Product
import com.develop.totvsapp.presentation.details.DetailsView
import com.develop.totvsapp.repositories.IntroRepositoryFromFile
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_comanda.view.*
import kotlinx.android.synthetic.main.item_list.view.*
import kotlinx.android.synthetic.main.item_list_comanda.view.*

class ComandaAdapter (  val products: ArrayList<Product>,
                        val context: Context,
                        val uidUser: String): RecyclerView.Adapter<ComandaAdapter.ViewHolder>(){

    lateinit var comandaView: ComandaView


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_list_comanda, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun getItem(position: Int): Any {
        return products[position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = products[position]
        //val product = getItem(position) as Product
        comandaView = ComandaView()

        holder?.let {

            it.title.text = product.title
            it.txtQtd.text = "Qtd : "+product.qtd
            it.txtPreco.text = "R$ : "+product.preco
            Picasso.get().load(product.image).into(it.image)

            it.btnDelete.setOnClickListener {


                val alertDialogBuilder = AlertDialog.Builder(context)
                alertDialogBuilder.setTitle("Deseja realmente excluir esse item ?")
                alertDialogBuilder.setMessage(product.title)
                alertDialogBuilder.setPositiveButton("Sim") { dialog, which ->
                    comandaView.deleteProductComanda(uidUser,product.idProd)
                }
                alertDialogBuilder.setNegativeButton("Não") { dialog, which ->

                }
                alertDialogBuilder.show()

            }

         /*   it.btnDetails.setOnClickListener {
                //Toast.makeText(context,"Detalhe",Toast.LENGTH_LONG).show()
                var i = Intent(context, DetailsView::class.java)
                i.putExtra("imageDetails",product.Image)
                i.putExtra("desc",product.desc)
                i.putExtra("idProd",product.idProd)
                i.putExtra("uidUser",uidUser)

                println(product.idProd)

                context.startActivity(i)
            }
        */

        }



    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title = itemView.tvComanda
        val image = itemView.imgComanda
        val btnDelete = itemView.btnRemover
        val txtQtd = itemView.tvQtd
        val txtPreco = itemView.tvPrecoComanda

        fun bindView(product: Product) {
            val title = itemView.textView2

            title.text = product.title

        }
    }

    }