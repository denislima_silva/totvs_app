package com.develop.totvsapp.presentation.intro

import com.develop.totvsapp.base.TotvsAppBaseContractPresenter
import com.develop.totvsapp.base.TotvsAppBaseContractView
import com.develop.totvsapp.models.Product
import com.develop.totvsapp.repositories.IntroRepositoryFromFile

class IntroContract {
    interface View: TotvsAppBaseContractView {
        fun adapterView(products: ArrayList<IntroRepositoryFromFile>)
    }
    interface Presenter: TotvsAppBaseContractPresenter{
        fun loadProducts()
    }
}