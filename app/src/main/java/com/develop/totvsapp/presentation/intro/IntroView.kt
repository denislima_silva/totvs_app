package com.develop.totvsapp.presentation.intro

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.develop.totvsapp.R
import com.develop.totvsapp.models.Product
import androidx.recyclerview.widget.LinearLayoutManager
import com.develop.totvsapp.presentation.comanda.ComandaView
import com.develop.totvsapp.presentation.main.Constants
import com.develop.totvsapp.repositories.IntroRepositoryFromFile
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_intro.*



class IntroView: AppCompatActivity(), IntroContract.View {

    lateinit var presenter: IntroContract.Presenter
    var recipeList = ArrayList<IntroRepositoryFromFile>()
    lateinit var uidUser: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        presenter = IntroPresenter(this)
        //presenter.loadProducts()

        val extras = intent.extras

        if (extras != null) {
            uidUser = extras.getString("uidUser")
        }

        btnLogoff.setOnClickListener {

            finish()
        }

        btnComanda.setOnClickListener {
            var i = Intent(this,ComandaView::class.java)
            i.putExtra("uidUser",uidUser)
            startActivity(i)
        }

        recipeList = IntroRepositoryFromFile.getRecipesFromFile(Constants.JSON_PROD, this)
        recyclerView.adapter = ProductAdapter(recipeList, this,uidUser)
        var layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
    }

    override fun adapterView(products: ArrayList<IntroRepositoryFromFile>){

        recyclerView.adapter = ProductAdapter(products, applicationContext,uidUser)
        var layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
    }

    override fun getLifeCycleView(): Lifecycle {
        return lifecycle
    }

    override fun getLifeCycleOwnerView(): LifecycleOwner {
        return this
    }

    override fun getActivityView(): FragmentActivity {
        return this
    }
}