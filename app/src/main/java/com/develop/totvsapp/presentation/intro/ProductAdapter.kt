package com.develop.totvsapp.presentation.intro

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.develop.totvsapp.R
import com.develop.totvsapp.models.Product
import com.develop.totvsapp.presentation.details.DetailsView
import com.develop.totvsapp.repositories.IntroRepositoryFromFile
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list.view.*





class ProductAdapter(val products: ArrayList<IntroRepositoryFromFile>,
                     val context: Context,
                     val uidUser: String): RecyclerView.Adapter<ProductAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun getItem(position: Int): Any {
        return products[position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //val product = products[position]
        val product = getItem(position) as IntroRepositoryFromFile

        holder?.let {
            it.title.text = product.Title
            it.preco.text = "R$ "+product.preco

            Picasso.get().load(product.Image).into(it.image)

            it.btnDetails.setOnClickListener {
                //Toast.makeText(context,"Detalhe",Toast.LENGTH_LONG).show()
                var i = Intent(context,DetailsView::class.java)
                i.putExtra("imageDetails",product.Image)
                i.putExtra("desc",product.desc)
                i.putExtra("idProd",product.idProd)
                i.putExtra("uidUser",uidUser)
                i.putExtra("title",product.Title)
                i.putExtra("preco",product.preco)

                context.startActivity(i)
            }


        }



    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title = itemView.textView2
        val image = itemView.img_vet
        val btnDetails = itemView.btnDetalhes
        val preco = itemView.tvPreco

        fun bindView(product: Product) {
            val title = itemView.textView2

            title.text = product.title

        }


    }

}