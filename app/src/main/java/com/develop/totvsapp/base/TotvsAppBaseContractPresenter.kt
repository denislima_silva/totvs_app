package com.develop.totvsapp.base

import androidx.lifecycle.LifecycleObserver

interface TotvsAppBaseContractPresenter: LifecycleObserver