package com.develop.totvsapp.base

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner

interface TotvsAppBaseContractView {
    fun getLifeCycleView(): Lifecycle
    fun getLifeCycleOwnerView(): LifecycleOwner
    fun getActivityView(): FragmentActivity
}