package com.develop.totvsapp.di

import android.content.Context
import com.develop.totvsapp.presentation.main.TotvsApp
import dagger.Module
import dagger.Provides

@Module
internal class ModuleContext {

    @Provides
    fun providesContext(): Context {
        return TotvsApp.instance.baseContext
    }


}