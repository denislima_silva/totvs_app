package com.develop.totvsapp.di

import com.develop.totvsapp.presentation.intro.IntroPresenter
import com.develop.totvsapp.presentation.main.TotvsApp
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ModuleContext::class])
interface ComponentApplication {
    fun inject(totvsApp: TotvsApp)
    fun inject(introPresenter: IntroPresenter)
}