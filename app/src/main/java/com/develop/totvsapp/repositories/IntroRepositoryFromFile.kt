package com.develop.totvsapp.repositories

import android.content.Context
import org.json.JSONException
import org.json.JSONObject

class IntroRepositoryFromFile (
    val idProd: String,
    val Title: String,
    val Image: String,
    val desc: String,
    val preco: String) {

        companion object {

            fun getRecipesFromFile(filename: String, context: Context): ArrayList<IntroRepositoryFromFile> {
                val recipeList = ArrayList<IntroRepositoryFromFile>()

                try {
                    // Load data
                    val jsonString = loadJsonFromAsset(filename, context)
                    val json = JSONObject(jsonString)
                    val recipes = json.getJSONArray("products")

                    // Get Recipe objects from data
                    (0 until recipes.length()).mapTo(recipeList) {
                        IntroRepositoryFromFile(recipes.getJSONObject(it).getString("idProd"),
                            recipes.getJSONObject(it).getString("Title"),
                            recipes.getJSONObject(it).getString("Image"),
                            recipes.getJSONObject(it).getString("desc"),
                            recipes.getJSONObject(it).getString("preco"))
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                return recipeList
            }

            private fun loadJsonFromAsset(filename: String, context: Context): String? {
                var json: String? = null

                try {
                    val inputStream = context.assets.open(filename)
                    val size = inputStream.available()
                    val buffer = ByteArray(size)
                    inputStream.read(buffer)
                    inputStream.close()
                    json = String(buffer, Charsets.UTF_8)
                } catch (ex: java.io.IOException) {
                    ex.printStackTrace()
                    return null
                }

                return json
            }
        }

}