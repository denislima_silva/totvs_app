package com.develop.totvsapp.repositories

import com.google.firebase.database.*

class ComandaRepository {

    private var database: DatabaseReference


    init {
        database = FirebaseDatabase.getInstance().reference
    }


    fun addProductComandaModel(uidUser: String, idProd: String, img: String,title: String,precoTot: Int,qtd: Int): Boolean{

        database.child("comanda").child(uidUser).child(idProd).child("qtd").setValue(qtd)
        database.child("comanda").child(uidUser).child(idProd).child("title").setValue(title)
        database.child("comanda").child(uidUser).child(idProd).child("image").setValue(img)
        database.child("comanda").child(uidUser).child(idProd).child("preco").setValue(precoTot)

        return true
    }

    fun deleteProductComanda(uidUser: String,idProd: String): Boolean{

        database.child("comanda").child(uidUser).child(idProd).removeValue()

        return true
    }

    fun paymentComanda(uidUser: String): Boolean{

        database.child("comanda").child(uidUser).removeValue()

        return true
    }

}