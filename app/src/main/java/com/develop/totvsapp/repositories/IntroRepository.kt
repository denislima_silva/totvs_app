package com.develop.totvsapp.repositories

import android.content.ContentValues.TAG
import android.util.Log
import com.develop.totvsapp.models.Product
import com.google.firebase.database.*

class IntroRepository {

    var productList: ArrayList<Product> = arrayListOf()

    lateinit var database: DatabaseReference

    fun getProducts(): ArrayList<Product> {

        database = FirebaseDatabase.getInstance().getReference().child("products")

        database.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                val post = p0.children
                post.forEach {
                    //println(it.child("Title"))
                    productList.add(Product(
                        it.child("Title").toString(),
                        it.child("Image").toString(),
                        it.child("idProd").toString(),
                        it.child("qtd").toString(),
                        it.child("preco").toString()))
                }
                //newList.addAll(post as Collection<Product>)
               //println(newList)
            }

        })


        return productList
    }


}